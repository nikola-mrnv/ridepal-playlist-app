export const generatePlaylist = {
  travelDuration: (value) => typeof value === 'number' && value > 0,
  uniqueArtists: (value) => typeof value === 'boolean',
  genres: (value) => Array.isArray(value) && value.length <= 2 && value.length > 0 && value.reduce((acc, genre) => acc + genre.amount, 0) === 100,
};
