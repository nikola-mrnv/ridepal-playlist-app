export const getRouteSchema = {
  origin: (value) => typeof value === 'string' && value.length > 3,
  destination: (value) => typeof value === 'string' && value.length > 3,
};
