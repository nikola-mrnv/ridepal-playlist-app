export const getTravelInformationSchema = {
  originPoint: value =>
    Array.isArray(value) &&
    value.length === 2 &&
    value[0] >= -90 &&
    value[0] <= 90 &&
    value[1] >= -180 &&
    value[1] <= 180 &&
    !Number.isNaN(value[0]) &&
    !Number.isNaN(value[1]),
  destinationPoint: value =>
    Array.isArray(value) &&
    value.length === 2 &&
    value[0] >= -90 &&
    value[0] <= 90 &&
    value[1] >= -180 &&
    value[1] <= 180 &&
    !Number.isNaN(value[0]) &&
    !Number.isNaN(value[1]),
};
