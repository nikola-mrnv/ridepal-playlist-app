export const saveNewPlaylist = {
  name: (value) => typeof value === 'string' && value.length > 3,
  hidden: (value) => typeof value === 'boolean',
  picture_url: (value) => typeof value === 'string' && value.length > 0,
  duration: (value) => typeof value === 'number' && value > 0,
  average_rank: (value) => typeof value === 'number' && value > 0,
  nb_tracks: (value) => typeof value === 'number' && value > 0,
  genres: (value) => Array.isArray(value) && value.length <= 2,
};
