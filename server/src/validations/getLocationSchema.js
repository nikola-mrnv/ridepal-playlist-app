export const getLocationSchema = {
  origin: value => typeof value === 'string' && value.length > 3,
  destination: value => typeof value === 'string' && value.length > 3,
};
