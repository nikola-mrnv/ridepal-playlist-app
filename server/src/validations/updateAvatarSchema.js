export const updateAvatarSchema = {
  originalname: value => (/\.(gif|jpe?g|png|webp|bmp)$/i).test(value),
  size: value => typeof value === 'number' && value < 1000000,
};