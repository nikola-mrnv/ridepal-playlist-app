import fetch from 'node-fetch';
import MAPS_CONSTANTS from '../common/maps-constants.js';

const getLocationData = async (location) => {
  const locationEncoded = encodeURI(location);

  const response = await fetch(`${MAPS_CONSTANTS.API_URL}/Locations/${locationEncoded}?key=${MAPS_CONSTANTS.API_KEY}`);
  const data = await response.json();
  return data.resourceSets[0];
};

const getDistanceData = async (originPoint, destinationPoint) => {
  const response = await fetch(
    `${MAPS_CONSTANTS.API_URL}/Routes/DistanceMatrix?origins=${originPoint.join(',')}&destinations=${destinationPoint.join(',')}&travelMode=${
      MAPS_CONSTANTS.API_TRAVEL_MODE
      // eslint-disable-next-line comma-dangle
    }&key=${MAPS_CONSTANTS.API_KEY}`
  );
  const data = await response.json();
  return data;
};

const getRouteData = async (origin, destination) => {
  const response = await fetch(`${MAPS_CONSTANTS.API_URL}/Routes?wp.0=${origin}&wp.1=${destination}&key=${MAPS_CONSTANTS.API_KEY}`);
  const data = await response.json();
  return data;
};

export default {
  getLocationData,
  getDistanceData,
  getRouteData,
};
