import db from './pool.js';

export const tokenExists = async (token) => {
  const result = await db.query('SELECT * FROM tokens WHERE token = ? ', [token]);

  return result && result.length > 0;
};
