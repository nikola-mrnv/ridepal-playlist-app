import userRoles from '../common/user-roles.js';
import pool from './pool.js';

const getAll = async (roleId = userRoles.User) => {
  const sql = `
  SELECT p.playlist_id, p.name, p.picture_url, p.is_hidden, p.duration, p.user_id, p.created_on, p.modified_on, p.is_deleted, (SELECT ROUND(AVG(t.rank)) FROM tracks t JOIN playlist_tracks_junction ptj ON t.dz_track_id = ptj.dz_track_id WHERE ptj.playlist_id = p.playlist_id) AS playlist_rank
  FROM playlists p
  JOIN playlist_tracks_junction ptj ON p.playlist_id = ptj.playlist_id
  JOIN tracks t ON t.dz_track_id = ptj.dz_track_id
  ${roleId === userRoles.Admin ? 'WHERE p.is_deleted IN (0,1)' : 'WHERE p.is_deleted IN (0)'}
  GROUP BY playlist_id
  ORDER BY playlist_rank DESC
  `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT p.playlist_id, p.name, p.picture_url, p.is_hidden, p.duration, p.user_id, p.created_on, p.modified_on, p.is_deleted, (SELECT ROUND(AVG(t.rank)) FROM tracks t JOIN playlist_tracks_junction ptj ON t.dz_track_id = ptj.dz_track_id WHERE ptj.playlist_id = p.playlist_id) AS playlist_rank
  FROM ridepaldb.playlists p
  WHERE ${column} = ?
  `;

  return (await pool.query(sql, [value]))[0];
};

const getAllBy = async (userId, roleId = userRoles.User) => {
  const sql = `
  SELECT p.playlist_id, p.name, p.picture_url, p.is_hidden, p.duration, p.user_id, p.created_on, p.modified_on, p.is_deleted, (SELECT ROUND(AVG(t.rank)) FROM tracks t JOIN playlist_tracks_junction ptj ON t.dz_track_id = ptj.dz_track_id WHERE ptj.playlist_id = p.playlist_id) AS playlist_rank
  FROM playlists p
  JOIN playlist_tracks_junction ptj ON p.playlist_id = ptj.playlist_id
  JOIN tracks t ON t.dz_track_id = ptj.dz_track_id
  ${roleId === userRoles.Admin ? 'WHERE p.is_deleted IN (0,1)' : 'WHERE p.is_deleted IN (0)'}
  AND p.user_id = ?
  GROUP BY playlist_id
  ORDER BY playlist_rank DESC
  `;

  return await pool.query(sql, [userId]);
};

const getTracksBy = async (column, value) => {
  const sql = `
  SELECT t.dz_track_id, t.title, t.duration, t.explicit_lyrics, t.link, t.preview_url, al.title AS album_title, ar.name AS artist_name, g.name AS genre_name
  FROM playlist_tracks_junction ptj
  JOIN tracks t ON ptj.dz_track_id = t.dz_track_id
  JOIN albums al ON t.dz_album_id = al.dz_album_id
  JOIN artists ar ON t.dz_artist_id = ar.dz_artist_id
  JOIN genres g ON t.dz_genre_id = g.dz_genre_id
  WHERE ${column} = ?
  `;

  return await pool.query(sql, [value]);
};

const edit = async (id, data) => {
  const sql = `
  UPDATE playlists SET name = ?, is_hidden = ? WHERE playlist_id = ?
  `;
  await pool.query(sql, [data.name, Number(data.hidden), id]);

  return await getBy('playlist_id', id);
};

const remove = async (id) => {
  const sql = `
  UPDATE playlists SET is_deleted = 1 WHERE playlist_id = ?
  `;
  await pool.query(sql, [id]);

  return await getBy('playlist_id', id);
};

const getGenres = async () => {
  const sql = `
  SELECT g.dz_genre_id, g.name, COUNT(t.dz_track_id) AS nb_tracks, g.picture_url
  FROM tracks t
  JOIN genres g ON g.dz_genre_id = t.dz_genre_id
  GROUP BY t.dz_genre_id
  HAVING COUNT(t.dz_track_id) > 400;
  `;

  return await pool.query(sql);
};

const getGenresByPlaylist = async (id) => {
  const sql = `
  SELECT g.dz_genre_id, g.name as name, g.picture_url, pgj.amount
  FROM playlists p
  JOIN playlist_genres_junction pgj ON pgj.playlist_id = p.playlist_id
  JOIN genres g ON g.dz_genre_id = pgj.dz_genre_id
  WHERE p.playlist_id = ?
`;

  return await pool.query(sql, [id]);
};

const generate = async (genreId) => {
  const sql = `
  SELECT t.dz_track_id, t.title, t.duration, t.rank, t.explicit_lyrics, t.link, t.preview_url, al.title AS album_title, ar.name AS artist_name, g.name AS genre_name
  FROM tracks t
  JOIN albums al ON t.dz_album_id = al.dz_album_id
  JOIN artists ar ON t.dz_artist_id = ar.dz_artist_id
  JOIN genres g ON t.dz_genre_id = g.dz_genre_id
  WHERE t.dz_genre_id = ?
  ORDER BY RAND()
  `;

  return await pool.query(sql, [genreId]);
};

const savePlaylist = async (name, duration, user_id, hidden, picture_url) => {
  const sql = hidden
    ? 'INSERT INTO playlists (name, is_hidden, duration, user_id, picture_url) VALUES (?, 1, ?, ?, ?)'
    : 'INSERT INTO playlists (name, duration, user_id, picture_url) VALUES (?, ?, ?, ?);';

  const result = await pool.query(sql, [name, duration, user_id, picture_url]);

  const sql2 = `
  SELECT * FROM playlists WHERE playlist_id = ?; 
  `;

  const createdPlaylist = await pool.query(sql2, [result.insertId]);

  return createdPlaylist[0];
};

const saveTracksJunction = async (playlistId, tracks) => {
  const transformedTracks = tracks.map(({ dz_track_id }) => `(${playlistId},${dz_track_id})`);
  const sql = `
  INSERT INTO playlist_tracks_junction (playlist_id, dz_track_id) VALUES ${transformedTracks.join(',')};
  `;

  await pool.query(sql);

  const sql2 = `
  SELECT * FROM playlist_tracks_junction WHERE playlist_id = ?;
  `;

  return await pool.query(sql2, [playlistId]);
};

const saveGenresJunction = async (playlistId, genres) => {
  const transformedGenres = genres.map(({ dz_genre_id, amount }) => `(${playlistId},${dz_genre_id},${amount})`);
  const sql = `
  INSERT INTO playlist_genres_junction (playlist_id, dz_genre_id, amount) VALUES ${transformedGenres.join(',')};
  `;

  await pool.query(sql);

  const sql2 = `
  SELECT * FROM playlist_genres_junction WHERE playlist_id = ?;
  `;

  return await pool.query(sql2, [playlistId]);
};

export default {
  getAll,
  getBy,
  getAllBy,
  getTracksBy,
  edit,
  remove,
  getGenres,
  generate,
  getGenresByPlaylist,
  savePlaylist,
  saveTracksJunction,
  saveGenresJunction,
};
