import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const options = {
  secretOrKey: SECRET_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const user = {
    user_id: payload.user_id,
    username: payload.username,
    role_id: payload.role_id,
  };

  done(null, user);
});

export default jwtStrategy;