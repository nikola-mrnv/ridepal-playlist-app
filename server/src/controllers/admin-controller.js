import express from 'express';
import { authMiddleware } from '../authentication/auth.middleware.js';
import userRoles from '../common/user-roles.js';
import usersData from '../data/users-data.js';
import loggedUserGuard from '../middleware/logged-user-guard.js';
import { roleAuthorization } from '../middleware/role-authorization.js';
import serviceErrors from '../services/service-errors.js';
import usersServices from '../services/users-services.js';

const adminController = express.Router();

adminController.use(authMiddleware);
adminController.use(loggedUserGuard);
adminController.use(roleAuthorization(userRoles.Admin));

adminController
  // get all users
  .get('/users', async (req, res) => {
    const { error, users } = await usersServices.getAllUsers(usersData)();

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No users found!' });
    } else {
      res.json(users);
    }
  })
  // delete user
  .delete('/users/:id', async (req, res) => {
    const { id } = req.params;
    if (req.user.user_id === +id) {
      return res.status(409).json({ error: 'You cannot delete yourself!' });
    }
    const { error, user } = await usersServices.deleteUser(usersData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'User not found!' });
    } else {
      res.json(user);
    }
  });

export default adminController;
