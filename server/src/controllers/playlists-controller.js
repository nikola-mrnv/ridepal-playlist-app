import express from 'express';
import { authMiddleware } from '../authentication/auth.middleware.js';
import playlistsData from '../data/playlists-data.js';
import bodyValidator from '../middleware/body-validator.js';
import loggedUserGuard from '../middleware/logged-user-guard.js';
import playlistsServices from '../services/playlists-services.js';
import serviceErrors from '../services/service-errors.js';
import { editPlaylistSchema } from '../validations/editPlaylistSchema.js';
import { generatePlaylist } from '../validations/generatePlaylistSchema.js';
import { saveNewPlaylist } from '../validations/saveNewPlaylistSchema.js';

const playlistsController = express.Router();

playlistsController
  .get('/', async (req, res) => {
    const { error, playlists } = await playlistsServices.getAllPlaylists(playlistsData)();

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ message: 'No playlists found!' });
    } else {
      res.json(playlists);
    }
  })
  .get('/registered/', authMiddleware, loggedUserGuard, async (req, res) => {
    const { role_id: roleId } = req.user;
    const { error, playlists } = await playlistsServices.getAllPlaylists(playlistsData)(+roleId);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ message: 'No playlists found!' });
    } else {
      res.json(playlists);
    }
  })
  .get('/genres', async (req, res) => {
    const { error, genres } = await playlistsServices.getAllGenres(playlistsData)();

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'Genres not found!' });
    } else {
      res.json(genres);
    }
  })
  .get('/:id', async (req, res) => {
    const { id } = req.params;

    const { error, playlist } = await playlistsServices.getPlaylistById(playlistsData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'Playlist not found!' });
    } else {
      res.json(playlist);
    }
  })
  .post('/generator', authMiddleware, loggedUserGuard, bodyValidator('playlist generator', generatePlaylist), async (req, res) => {
    const { travelDuration, uniqueArtists, genres } = req.body;
    const { error, playlist } = await playlistsServices.generatePlaylist(playlistsData)(+travelDuration, uniqueArtists, genres);

    if (error === serviceErrors.INVALID_RESPONSE) {
      return res.status(418).json({ message: 'Playlist was shorter or longer than required!' });
    } else {
      res.json(playlist);
    }
  })
  .post('/new-playlist', authMiddleware, loggedUserGuard, bodyValidator('save new playlist', saveNewPlaylist), async (req, res) => {
    const { name, duration, tracks, genres, hidden, picture_url } = req.body;
    const { user_id } = req.user;
    const newPlaylist = await playlistsServices.saveNewPlaylist(playlistsData)(name, +duration, user_id, tracks, genres, hidden, picture_url);

    res.json({ ...newPlaylist, message: 'Successfully generated playlist!' });
  })
  .put('/:id', authMiddleware, loggedUserGuard, bodyValidator('playlist', editPlaylistSchema), async (req, res) => {
    const { id } = req.params;

    const { error, playlist } = await playlistsServices.editPlaylist(playlistsData)(req.user, req.body, +id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'Playlist not found!' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ error: 'You are not authorized!' });
    } else {
      res.json(playlist);
    }
  })
  .delete('/:id', authMiddleware, loggedUserGuard, async (req, res) => {
    const { id } = req.params;

    const { error, playlist } = await playlistsServices.deletePlaylist(playlistsData)(req.user, +id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'Playlist not found!' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ error: 'You are not authorized!' });
    } else {
      res.json(playlist);
    }
  });

export default playlistsController;
