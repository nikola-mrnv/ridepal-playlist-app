import serviceErrors from './service-errors.js';
import userRoles from '../common/user-roles.js';
import bcrypt from 'bcrypt';
import createToken from '../authentication/create-token.js';

const createUser = usersData => {
  return async userData => {
    const { username, password, email } = userData;
    const existingUser = await usersData.getBy('username', username);
    if (existingUser.username) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const existingEmail = await usersData.getBy('email', email);
    if (existingEmail.email) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const newUser = await usersData.create(username, email, await bcrypt.hash(password, 10), userRoles.User);

    return { error: null, user: newUser };
  };
};

const loginUser = usersData => {
  return async data => {
    const { username, password } = data;
    const existingUser = await usersData.getUserWithPassword('username', username);
    if (!existingUser.username) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        token: null,
      };
    }

    if (!(await bcrypt.compare(password, existingUser.password))) {
      return {
        error: serviceErrors.INVALID_PASSWORD,
        token: null,
      };
    }

    const token = createToken({
      user_id: existingUser.user_id,
      username: existingUser.username,
      role_id: existingUser.role_id,
    });

    return { error: null, token: token };
  };
};

const logoutUser = usersData => {
  return async token => {
    await usersData.addToken(token);
  };
};

const getAllUsers = usersData => {
  return async () => {
    const users = await usersData.getAll();
    if (users.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        users: null,
      };
    }

    return { error: null, users: users };
  };
};

const deleteUser = usersData => {
  return async id => {
    const user = await usersData.getBy('user_id', id);
    if (!user.user_id || user.is_deleted === 1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    await usersData.remove(id);

    const deletedUser = await usersData.getBy('user_id', id);

    return { error: null, user: deletedUser };
  };
};

const getUserById = usersData => {
  return async (id, user) => {
    if (user.user_id !== id && user.role_id !== userRoles.Admin) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        user: null,
      };
    }

    const foundUser = await usersData.getBy('user_id', id);

    if (!foundUser.user_id) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return { error: null, user: foundUser };
  };
};

const updateUser = usersData => {
  return async (user, data, id) => {
    const existingUser = await usersData.getBy('user_id', id);

    if (!existingUser.user_id) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    if (user.user_id !== id && user.role_id !== userRoles.Admin) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        user: null,
      };
    }

    const updatedUser = await usersData.update(data, id);

    return { error: null, user: updatedUser };
  };
};

const updateUserAvatar = usersData => {
  return async (user, filename, id) => {
    const existingUser = await usersData.getBy('user_id', id);

    if (!existingUser.user_id) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    if (user.user_id !== id && user.role_id !== userRoles.Admin) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        user: null,
      };
    }

    const updatedUser = await usersData.updateAvatar(filename, id);

    return { error: null, updatedAvatar: updatedUser.avatar_url };
  };
};

export default {
  createUser,
  loginUser,
  logoutUser,
  getAllUsers,
  deleteUser,
  getUserById,
  updateUser,
  updateUserAvatar,
};
