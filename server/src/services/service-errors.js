export default {
  /** Such a record does not exist */
  RECORD_NOT_FOUND: 1,
  /** The requirements do not allow more than one of that resource */
  DUPLICATE_RECORD: 2,
  /** The requirements do not allow such an operation */
  OPERATION_NOT_PERMITTED: 3,
  /** Invalid password was provided for user login */
  INVALID_PASSWORD: 4,
  /** Such username does not exist */
  INVALID_USERNAME: 5,
  /** The response does not meet the requirements */
  INVALID_RESPONSE: 6,
};
