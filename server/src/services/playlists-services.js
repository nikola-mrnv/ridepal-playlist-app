import { PLAYLIST_ROUNDING } from '../common/constants.js';
import userRoles from '../common/user-roles.js';
import serviceErrors from './service-errors.js';

const getAllPlaylists = (playlistsData) => {
  return async (roleId) => {
    const playlists = await playlistsData.getAll(roleId);
    if (playlists.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlists: null,
      };
    }

    const playlistsWithGenres = [];

    for (const playlist of playlists) {
      const genres = await playlistsData.getGenresByPlaylist(playlist.playlist_id);
      playlistsWithGenres.push({ ...playlist, genres: genres });
    }

    return { error: null, playlists: playlistsWithGenres };
  };
};

const getPlaylistById = (playlistsData) => {
  return async (id) => {
    const foundPlaylist = await playlistsData.getBy('playlist_id', id);
    if (!foundPlaylist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    const foundTracks = await playlistsData.getTracksBy('playlist_id', id);
    if (foundTracks.length === 0) {
      return { error: null, playlist: { ...foundPlaylist, nb_tracks: foundTracks.length, tracks: null } };
    }

    return { error: null, playlist: { ...foundPlaylist, nb_tracks: foundTracks.length, tracks: foundTracks } };
  };
};

const getAllPlaylistsByUser = (playlistsData) => {
  return async (userId, roleId, loggedUserId) => {
    const playlists = await playlistsData.getAllBy(userId, roleId);
    if (playlists.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlists: null,
      };
    }

    if (loggedUserId !== playlists[0].user_id && roleId !== userRoles.Admin) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        playlist: null,
      };
    }

    const playlistsWithGenres = [];

    for (const playlist of playlists) {
      const genres = await playlistsData.getGenresByPlaylist(playlist.playlist_id);
      playlistsWithGenres.push({ ...playlist, genres: genres });
    }

    return { error: null, playlists: playlistsWithGenres };
  };
};

const editPlaylist = (playlistsData) => {
  return async (user, data, id) => {
    const playlist = await playlistsData.getBy('playlist_id', id);
    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    if (user.user_id !== playlist.user_id && user.role_id !== userRoles.Admin) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        playlist: null,
      };
    }

    const editedPlaylist = await playlistsData.edit(id, data);

    const genres = await playlistsData.getGenresByPlaylist(id);

    return { error: null, playlist: { ...editedPlaylist, genres: genres } };
  };
};

const deletePlaylist = (playlistsData) => {
  return async (user, id) => {
    const playlist = await playlistsData.getBy('playlist_id', id);
    if (!playlist || playlist.is_deleted) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlistId: null,
      };
    }

    if (user.user_id !== playlist.user_id && user.role_id !== userRoles.Admin) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        playlistId: null,
      };
    }

    await playlistsData.remove(id);

    const deletedPlaylist = await playlistsData.getBy('playlist_id', id);

    return { error: null, playlist: deletedPlaylist };
  };
};

const getAllGenres = (playlistsData) => {
  return async () => {
    const genres = await playlistsData.getGenres();

    if (genres.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        genres: null,
      };
    }

    return { error: null, genres: genres };
  };
};

const generatePlaylist = (playlistsData) => {
  return async (travelDuration, uniqueArtists, genres) => {
    const playlist = [];

    const shuffle = (arr) => {
      let tempValue, randomIndex;

      for (let i = arr.length - 1; i >= 0; i--) {
        randomIndex = Math.floor(Math.random() * (i + 1));

        tempValue = arr[i];
        arr[i] = arr[randomIndex];
        arr[randomIndex] = tempValue;
      }

      return arr;
    };

    for (const genre of genres) {
      const genreTime = travelDuration * (genre.amount / 100);
      const generatedTracks = await playlistsData.generate(genre.dz_genre_id);

      if (uniqueArtists === true) {
        const uniqueArtistTracks = [
          ...new Map(shuffle(generatedTracks.map((track) => track)).map((track) => [track['artist_name'], track])).values(),
        ];
        while (uniqueArtistTracks.reduce((acc, track) => acc + track.duration, 0) > genreTime + PLAYLIST_ROUNDING / genres.length) {
          shuffle(uniqueArtistTracks).pop();
        }

        playlist.push(...uniqueArtistTracks);
      } else {
        const nonUniqueArtistTracks = shuffle(generatedTracks.map((track) => track));
        while (nonUniqueArtistTracks.reduce((acc, track) => acc + track.duration, 0) > genreTime + PLAYLIST_ROUNDING / genres.length) {
          shuffle(nonUniqueArtistTracks).pop();
        }

        playlist.push(...nonUniqueArtistTracks);
      }
    }
    const playlistDuration = playlist.reduce((acc, track) => acc + track.duration, 0);
    const playlistRank = Math.round(playlist.reduce((acc, track) => acc + track.rank, 0) / playlist.length);

    if (playlistDuration > travelDuration + PLAYLIST_ROUNDING || playlistDuration < travelDuration - PLAYLIST_ROUNDING) {
      return { error: serviceErrors.INVALID_RESPONSE, playlist: null };
    }

    return {
      error: null,
      playlist: { duration: playlistDuration, average_rank: playlistRank, nb_tracks: playlist.length, genres, tracks: shuffle(playlist) },
    };
  };
};

const saveNewPlaylist = (playlistsData) => {
  return async (name, duration, user_id, tracks, genres, hidden, picture_url) => {
    const savedPlaylist = await playlistsData.savePlaylist(name, duration, user_id, hidden, picture_url);
    const savedTracks = await playlistsData.saveTracksJunction(savedPlaylist.playlist_id, tracks);
    const savedGenres = await playlistsData.saveGenresJunction(savedPlaylist.playlist_id, genres);

    return {
      playlist: savedPlaylist,
      tracks: savedTracks,
      genres: savedGenres,
    };
  };
};

export default {
  getAllPlaylists,
  getPlaylistById,
  getAllPlaylistsByUser,
  editPlaylist,
  deletePlaylist,
  getAllGenres,
  generatePlaylist,
  saveNewPlaylist,
};
