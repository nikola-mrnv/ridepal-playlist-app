import serviceErrors from './service-errors.js';

const getLocations = (mapsData) => {
  return async (origin, destination) => {
    const originData = (await mapsData.getLocationData(origin)).resources;
    const destinationData = (await mapsData.getLocationData(destination)).resources;

    const parseOriginData = () =>
      originData
        .filter((origin) => origin.confidence !== 'Low')
        .map((origin) => ({
          originName: origin.name,
          originPoint: origin.geocodePoints[0].coordinates,
          entityType: origin.entityType,
        }));

    const parseDestinationData = () =>
      destinationData
        .filter((origin) => origin.confidence !== 'Low')
        .map((destination) => ({
          destinationName: destination.name,
          destinationPoint: destination.geocodePoints[0].coordinates,
          entityType: destination.entityType,
        }));

    if (originData.length === 0 && destinationData.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        origins: null,
        destinations: null,
      };
    } else if (originData.length === 0 && destinationData.length !== 0) {
      const destinations = parseDestinationData();
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        origins: null,
        destinations,
      };
    } else if (originData.length !== 0 && destinationData.length === 0) {
      const origins = parseOriginData();
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        origins,
        destinations: null,
      };
    } else {
      const destinations = parseDestinationData();
      const origins = parseOriginData();
      return {
        error: null,
        origins,
        destinations,
      };
    }
  };
};

const getDistanceMatrix = (mapsData) => {
  return async (originPoint, destinationPoint) => {
    const distanceMatrix = await mapsData.getDistanceData(originPoint, destinationPoint);

    if (distanceMatrix.resourceSets.length === 0) {
      return {
        error: distanceMatrix.errorDetails[0],
        travelDistanceInKilometers: null,
        travelDurationInMinutes: null,
      };
    }

    if (distanceMatrix.resourceSets[0].resources[0].results[0].travelDistance === -1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        travelDistanceInKilometers: null,
        travelDurationInMinutes: null,
      };
    }

    return {
      error: null,
      travelDistanceInKilometers: distanceMatrix.resourceSets[0].resources[0].results[0].travelDistance,
      travelDurationInMinutes: distanceMatrix.resourceSets[0].resources[0].results[0].travelDuration,
    };
  };
};

const getRoute = (mapsData) => {
  return async (origin, destination) => {
    const route = await mapsData.getRouteData(origin, destination);

    if (route.resourceSets.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        distanceUnit: null,
        durationUnit: null,
        travelDistance: null,
        travelDuration: null,
      };
    }

    return {
      error: null,
      distanceUnit: route.resourceSets[0].resources[0].distanceUnit,
      durationUnit: route.resourceSets[0].resources[0].durationUnit,
      startLocation: route.resourceSets[0].resources[0].routeLegs[0].startLocation.address,
      endLocation: route.resourceSets[0].resources[0].routeLegs[0].endLocation.address,
      travelDistance: route.resourceSets[0].resources[0].travelDistance,
      travelDuration: route.resourceSets[0].resources[0].travelDuration,
    };
  };
};

export default {
  getLocations,
  getDistanceMatrix,
  getRoute,
};
