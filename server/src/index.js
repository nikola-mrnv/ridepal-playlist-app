import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './authentication/strategy.js';
import adminController from './controllers/admin-controller.js';
import usersController from './controllers/users-controller.js';
import playlistsController from './controllers/playlists-controller.js';
import mapsController from './controllers/maps-controller.js';
import dotenv from 'dotenv';

const app = express();
const PORT = dotenv.config().parsed.PORT;

app.use(cors());
app.use(helmet());
app.use(express.json());

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use('/v1/admin', adminController);
app.use('/v1/users', usersController);
app.use('/v1/playlists', playlistsController);
app.use('/v1/maps', mapsController);
app.use('/v1/uploads', express.static('uploads'));

// return message if endopoint does not exist
app.all('*', (req, res) =>
  res.status(404).send({ error: 'Resource not found!' }),
);

app.listen(PORT, () => console.log(`Listening on ${PORT}...`));
