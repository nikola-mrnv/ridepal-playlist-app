import db from '../server/src/data/pool.js';
import fetch from 'node-fetch';
import bcrypt from 'bcrypt';
import userRoles from '../server/src/common/user-roles.js';


(async () => {

  // Insert Roles
  const roles = [
    { name: 'Admin' },
    { name: 'User' },
  ];

  await Promise.all(roles.map(({ name }) => db.query(`INSERT INTO roles (name)
  VALUES (?)`, [name])));

  // Insert admin
  console.log('Creating admin...');

  await db.query(`
      INSERT INTO users (username, display_name, email, password, role_id)
      VALUES (?, ?, ?, ?, ?)
    `, ['admin', 'admin', 'admin@playpal.com', await bcrypt.hash('password', 10), userRoles.Admin]);
 
  console.log('Admin is created');

  // Fetch Genres
  const getGenres = await fetch('https://api.deezer.com/genre');
  const genres = await getGenres.json();

  console.log('Getting genres...');
  const transformedGenres = genres.data.map(({ id, name, picture }) => `('${id}','${name}','${picture}')`);
  const sql = `INSERT INTO genres (dz_genre_id, name, picture_url) VALUES ${transformedGenres.slice(1).join(',')};`;

  try {
    await db.query(sql);
  } catch (error) {
    console.log(error);
  }

  console.log('Genres uploaded to database!');

  // Fetch Artists
  const getArtistFromGenre = async (genreId) => {
    const response = await fetch(`https://api.deezer.com/genre/${genreId}/artists`);
    const artists = await response.json();

    const transformedArtists = artists.data.map(({ id, name, picture, tracklist }) => `(${id},'${name.replace(/'/g, '')}','${picture}','${tracklist}',${genreId})`);
    const sql = `INSERT INTO artists (dz_artist_id, name, picture_url, tracklist_url, dz_genre_id) VALUES ${transformedArtists.join(',')};`;

    try {
      await db.query(sql);
    } catch (error) {
      console.log('Skipped artist - already exists');
    }
  };

  const sqlGenres = 'SELECT dz_genre_id FROM ridepaldb.genres';
  const responseGenres = await db.query(sqlGenres);
  const genreIds = responseGenres.map(({ dz_genre_id }) => dz_genre_id);

  console.log('Getting artists...');

  for (const genreId of genreIds) {
    await getArtistFromGenre(genreId);
  }

  console.log('Artists uploaded to database!');

  // Fetch albums

  const getAlbumsFromArtist = async ({ artistId, genreId }) => {
    const response = await fetch(`https://api.deezer.com/artist/${artistId}/albums`);
    const albums = await response.json();

    const transformedAlbums = albums.data
      .map(({ id, title, cover, tracklist, genre_id }) =>
        `(${id},'${title.replace(/'/g, '').slice(0, 100)}','${cover}','${tracklist}',${genre_id < 0 ? genreId : genre_id}, ${artistId})`);
    const sql = `INSERT INTO albums (dz_album_id, title, cover_url, tracklist_url, dz_genre_id, dz_artist_id) VALUES ${transformedAlbums.slice(0, 3).join(',')};`;

    try {
      await db.query(sql);
    } catch (error) {
      console.log('Skipped album - duplicate or invalid id');
    }
  };

  const sqlArtists = 'SELECT dz_artist_id, dz_genre_id FROM ridepaldb.artists;';
  const responseArtists = await db.query(sqlArtists);
  const artists = responseArtists.map(({ dz_artist_id, dz_genre_id }) => ({ artistId: dz_artist_id, genreId: dz_genre_id }));

  console.log('Getting albums...');

  for (const artist of artists) {
    await getAlbumsFromArtist(artist);
  }

  console.log('Albums uploaded to database!');

  // Fetch tracks

  const getTracksFromAlbums = async ({ albumId, genreId, artistId }) => {
    const response = await fetch(`https://api.deezer.com/album/${albumId}/tracks`);
    const tracks = await response.json();
    
    const transformedTracks = tracks.data.map(({ id, title, duration, rank, explicit_lyrics, link, preview }) =>
      `(${id},'${title.replace(/'/g, '').slice(0, 100)}',${duration},${rank},${explicit_lyrics}, '${link}','${preview}',${albumId},${artistId},${genreId})`);
    const sql = `INSERT INTO tracks (dz_track_id, title, duration, rank, explicit_lyrics, link, preview_url,dz_album_id, dz_artist_id, dz_genre_id) VALUES ${transformedTracks.join(',')};`;

    try {
      await db.query(sql);
    } catch (error) {
      console.log('Skipped track');
    }
  };

  const sqlAlbums = 'SELECT dz_album_id, dz_genre_id, dz_artist_id FROM ridepaldb.albums';
  const responseAlbums = await db.query(sqlAlbums);
  const albums = responseAlbums.map(({ dz_album_id, dz_artist_id, dz_genre_id }) => ({ albumId: dz_album_id, genreId: dz_genre_id, artistId: dz_artist_id }));

  console.log('Getting tracks...');

  for (const album of albums) {
    await getTracksFromAlbums(album);
  }

  console.log('Tracks uploaded to database!');

  db.end();
})();
