export const editPlaylistValidationSchema = (yup) => {
  return yup.object({
    name: yup.string('Enter playlist name').min(4, 'Playlist name should be of minimum 4 characters length').required('Playlist name is required'),
  });
};
