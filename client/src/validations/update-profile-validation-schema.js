export const updateProfileValidationSchema = yup => {
  return yup.object({
    display_name: yup
      .string('Enter a display name')
      .min(3, 'Display name should be of minimum 4 characters length')
      .required('Display name is required'),
    email: yup
      .string('Enter your email')
      .email('Enter a valid email address')
      .required('Email is required'),
  });
};
