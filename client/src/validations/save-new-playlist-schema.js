export const saveNewPlaylistSchema = (yup) => {
  return yup.object({
    name: yup.string('Enter playlist name').min(4, 'Name should be of minimum 4 characters length').required('Playlist name is required'),
  });
};
