export const loginValidationSchema = yup => {
  return yup.object({
    username: yup.string('Enter your username').min(4, 'Username should be of minimum 4 characters length').required('Username is required'),
    password: yup.string('Enter your password').min(6, 'Password should be of minimum 6 characters length').required('Password is required'),
  });
};
