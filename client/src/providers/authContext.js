import { createContext } from 'react';
import decode from 'jwt-decode';

const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setLoginState: () => { },
});

export const getToken = () => {
  return (localStorage.getItem('token') || '');
};

export const extractUser = token => {
  try {
    return decode(token);
  } catch (error) {
    return null;
  }
};

export default AuthContext;
