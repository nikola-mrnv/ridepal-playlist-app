import { AppBar, Grid, makeStyles, Tab, Tabs } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { CONSTANTS } from '../../common/constants.js';
import AdminPlaylistList from '../../components/AdminListItem/AdminPlaylistList.js';
import AdminUserList from '../../components/AdminListItem/AdminUserList.js';
import Navbar from '../../components/Navbar/Navbar.js';
import TabPanel from '../../components/TabPanel/TabPanel.js';
import { getToken } from '../../providers/authContext.js';
import { adminRequests } from '../../services/admin-requests.js';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#ccc',
  },
  gridContainer: {
    padding: '15px',
    margin: 0,
    width: '100%',
  },
}));

const Admin = () => {
  const [playlists, setPlaylists] = useState([]);
  const [users, setUsers] = useState([]);
  const [value, setValue] = useState(0);
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    adminRequests.fetchAllPlaylists(setPlaylists);
    adminRequests.fetchAllUsers(setUsers);
  }, []);

  return (
    <div>
      <Navbar />
      <h1 style={{ textAlign: 'center' }}>Admin Dashboard</h1>
      <Grid container className={classes.gridContainer}>
        <div className={classes.root}>
          <AppBar position="static">
            <Tabs value={value} onChange={handleChange} centered variant="fullWidth">
              <Tab label="Playlists" />
              <Tab label="Users" />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <AdminPlaylistList playlists={playlists} setPlaylists={setPlaylists} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <AdminUserList users={users} setUsers={setUsers} />
          </TabPanel>
        </div>
      </Grid>
    </div>
  );
};

export default Admin;
