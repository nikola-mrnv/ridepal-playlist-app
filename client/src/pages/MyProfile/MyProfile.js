import { Grid, makeStyles, Paper, useMediaQuery, useTheme } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import React, { useContext } from 'react';
import MyPlaylists from '../../components/MyPlaylists/MyPlaylists';
import Navbar from '../../components/Navbar/Navbar';
import UserProfile from '../../components/UserProfile/UserProfile';
import AuthContext from '../../providers/authContext';

const useStyles = makeStyles({
  root: {},
  gridContainer: {
    margin: 0,
    width: '100%',
    height: 'calc(100% - 65px)',
    backgroundColor: grey[300],
  },
});

const MyProfile = () => {
  const classes = useStyles();
  const auth = useContext(AuthContext);

  const theme = useTheme();
  const isSmall = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <>
      <Navbar />
      <Grid container spacing={3} className={classes.gridContainer}>
        <Grid item container xs={12} md={6} style={{ alignContent: 'flex-start' }}>
          <UserProfile user={auth.user} />
        </Grid>
        <Grid
          item
          container
          xs={12}
          md={6}
          component={Paper}
          style={{
            height: '100%',
            overflow: !isSmall && 'auto',
          }}
        >
          <MyPlaylists user={auth.user} withImage={true} fromProfile={true} />
        </Grid>
      </Grid>
    </>
  );
};

export default MyProfile;
