import { Grid, makeStyles, Paper, Typography, useTheme, useMediaQuery } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { playlistRequests } from '../../services/playlist-requests';
import PlaylistList from '../../components/PlaylistList/PlaylistList';
import Navbar from '../../components/Navbar/Navbar';
import FilterByGenre from '../../components/FilterByGenre/FilterByGenre';
import Sort from '../../components/Sort/Sort';
import AuthContext from '../../providers/authContext';
import { grey } from '@material-ui/core/colors';
import Quote from '../../components/Quote/Quote';
import HomeImage from '../../components/HomeImage/HomeImage';
import MyPlaylists from '../../components/MyPlaylists/MyPlaylists';
import Title from '../../components/Title/Title';

const useStyles = makeStyles({
  root: {
  },
  gridContainer: {
    margin: 0,
    width: '100%',
    height: '100vh',
    backgroundColor: grey[300],
  },
});

const Home = () => {

  const classes = useStyles();
  const [playlists, setPlaylists] = useState([]);
  const [initialPlaylists, setInitialPlaylists] = useState([]);
  const [loading, setLoading] = useState(true);
  const auth = useContext(AuthContext);

  const theme = useTheme();
  const isSmall = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    playlistRequests.getAllPlaylists(setPlaylists, setLoading, setInitialPlaylists);
  }, []);


  const filterByGenre = genre => {
    if (!genre) {
      setPlaylists(initialPlaylists);
    } else {
      const filteredPlaylists = initialPlaylists
        .filter(playlist => Object.values(playlist.genres)
          .map(g => g.dz_genre_id)
          .includes(genre.dz_genre_id));
      setPlaylists(filteredPlaylists);
    }
  };

  const sortPlaylists = order => {
    const sortedData = [...playlists];

    if (order === 'rankUp') {
      sortedData.sort((a, b) => a.playlist_rank - b.playlist_rank);
    }
    if (order === 'rankDown') {
      sortedData.sort((a, b) => b.playlist_rank - a.playlist_rank);
    }
    if (order === 'durationUp') {
      sortedData.sort((a, b) => a.duration - b.duration);
    }
    if (order === 'durationDown') {
      sortedData.sort((a, b) => b.duration - a.duration);
    }
    if (order === 'nameAsc') {
      sortedData.sort((a, b) => a.name.localeCompare(b.title));
    }
    if (order === 'nameDesc') {
      sortedData.sort((a, b) => b.name.localeCompare(a.title));
    }
    setPlaylists(sortedData);
  };

  return (
    <>
      {auth.user && <Navbar />}
      <Grid container
        spacing={3}
        className={classes.gridContainer}
        style={{ height: auth.user && 'calc(100% - 65px)' }}
      >
        <Grid item container
          xs={12} md={8}
          style={{
            alignContent: 'flex-start',
            justifyContent: 'center',
            height: '100%',
            overflow: !isSmall && 'auto',
          }}>
          {!auth.user &&
            <div style={{ flexDirection: 'column', width: '100%' }}>
              <Title />
              <Quote />
              <Grid item xs={12} sm={6}
                style={{ padding: '0 12px', marginLeft: 'auto' }}
              >
                <FilterByGenre
                  filterByGenre={filterByGenre} />
              </Grid>
            </div>}
          {auth.user &&
            <>
              <Typography
                component="h2"
                variant="h5"
                gutterBottom
                style={{ fontSize: '2.3em' }}
              >
                Discover New Music
                </Typography>
              <Grid item container
                xs={12} spacing={3}
                style={{ margin: 0 }}>
                <Grid item
                  xs={12} sm={4} lg={4}>
                  <FilterByGenre
                    filterByGenre={filterByGenre} />
                </Grid>
                <Grid item xs={12} sm={8} lg={8}>
                  <Sort
                    sortPlaylists={sortPlaylists} />
                </Grid>
              </Grid>
            </>}
          <Grid item container
            xs={12}
            spacing={3}
            style={{ margin: 0 }}>
            <PlaylistList
              loading={loading}
              playlists={playlists.filter(p => p.is_hidden === 0)}
              withImage={true} />
          </Grid>

        </Grid>
        {auth.user
          ? <Grid
            item container
            xs={12} md={4}
            component={Paper}
            style={{
              height: '100%',
              overflow: !isSmall && 'auto',
            }}
          >
            <MyPlaylists user={auth.user} withImage={false} />
          </Grid>
          : !isSmall && <HomeImage />}
      </Grid>
    </>
  );

};

export default Home;
