import React, { useContext } from 'react';
import AuthContext from '../../providers/authContext.js';
import { AppBar, Toolbar, Typography, Button, makeStyles } from '@material-ui/core';
import { handleLogout } from '../../services/user-requests.js';
import { useHistory, withRouter } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  margin: {
    marginLeft: '1em',
  },
}));

const Navbar = (props) => {

  const auth = useContext(AuthContext);
  const classes = useStyles();
  const history = useHistory();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            PlayPal
          </Typography>
          <Button className={classes.margin} color="secondary" variant="contained" onClick={() => history.push('/new-playlist')}>
            Create New Playlist
          </Button>
          <Button className={classes.margin} color="inherit" onClick={() => history.push('/')}>
            Home
          </Button>
          {auth.user.role_id === 1 && (<Button className={classes.margin} color="inherit" onClick={() => history.push('/admin')}>
            Admin
          </Button>)}
          {auth.user && (<Button className={classes.margin} color="inherit" onClick={() => history.push('/myprofile')}>
            My Profile
          </Button>)}
          <Button className={classes.margin} color="inherit" onClick={auth.user ? () => handleLogout(auth) : () => props.history.push('/login')}>
            {auth.user ? 'Logout' : 'Login'}
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default withRouter(Navbar);
