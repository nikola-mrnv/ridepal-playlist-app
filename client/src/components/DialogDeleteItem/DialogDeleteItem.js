import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import React from 'react';

const DialogDeleteItem = props => {
  return (
    <Dialog
      open={props.alertDialog}
      onClose={props.handleClickClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Are you sure?'}</DialogTitle>
      <DialogContent id="alert-dialog-description">{`Please confirm you want to delete this ${props.itemInfo}.`}</DialogContent>
      <DialogActions>
        <Button onClick={props.handleClickClose} variant="contained">
          Cancel
        </Button>
        {props.itemInfo === 'playlist' && (
          <Button onClick={() => props.handleDeletePlaylist(props.playlist_id)} variant="contained" color="secondary">
            Delete Playlist
          </Button>
        )}
        {props.itemInfo === 'user' && (
          <Button onClick={() => props.handleDeleteUser(props.user_id)} variant="contained" color="secondary">
            Delete User
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default DialogDeleteItem;
