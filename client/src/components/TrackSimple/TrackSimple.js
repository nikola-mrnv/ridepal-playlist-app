import React, { makeStyles, TableCell, TableRow, Typography } from '@material-ui/core';
import { utils } from '../../common/utils';
import GraphicEqIcon from '@material-ui/icons/GraphicEq';
import { useContext } from 'react';
import AuthContext from '../../providers/authContext';

const useStyles = makeStyles({
  root: {
    padding: '8px',
  },
});

const TrackSimple = ({ track, index, page, rowsPerPage, currentTrack, playTrack }) => {

  const classes = useStyles();
  const formattedDuration = utils.formatDuration(track.duration).substr(3, 5);
  const auth = useContext(AuthContext);

  return (
    <TableRow
      onClick={auth.user && (() => playTrack(track.dz_track_id))}
      style={{ cursor: auth.user && 'pointer' }}
      hover
      role="checkbox"
      tabIndex={- 1}
    >
      <TableCell
        className={classes.root}
        style={{ width: '20px' }}
      >
        <Typography variant="body2" align="center">
          {currentTrack.dz_track_id === track.dz_track_id
            ? <GraphicEqIcon fontSize="small" />
            : `${rowsPerPage * page + index + 1}.`
          }
        </Typography>
      </TableCell>
      <TableCell
        className={classes.root}
      >
        <Typography variant="body2" style={{ fontWeight: currentTrack.dz_track_id === track.dz_track_id ? 'bold' : 'initial' }}>
          {track.title}
        </Typography>
      </TableCell>
      <TableCell className={classes.root}>
        {track.artist_name}
      </TableCell>
      <TableCell className={classes.root}>
        {track.album_title}
      </TableCell>
      <TableCell className={classes.root}>
        {formattedDuration}
      </TableCell>
    </TableRow >
  );

};

export default TrackSimple;
