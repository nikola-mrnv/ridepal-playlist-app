import React from 'react';
import { Redirect, Route } from 'react-router';

const GuardedRoute = ({ component: Component, isLoggedIn, ...rest }) => {
  return <Route {...rest} render={props => (isLoggedIn ? <Component {...props} /> : <Redirect to="/" />)} />;
};

export default GuardedRoute;
