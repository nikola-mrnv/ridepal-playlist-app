import React, { useEffect, useState } from 'react';
import { makeStyles, Paper } from '@material-ui/core';
import Player from '../Player/Player';
import Loading from '../Loading/Loading';
import { playlistRequests } from '../../services/playlist-requests';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 1000,
  },
});

const PlayerContainer = ({ playlistId }) => {

  const classes = useStyles();
  const [playlist, setPlaylist] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    playlistRequests.getPlaylistById(playlistId, setPlaylist, setLoading);
  }, []);

  return (
    <>
      {loading
        ? <Paper className={classes.root}>
          <Loading />
        </Paper>
        : <Player playlist={playlist} />
      }
    </>
  );
};

export default PlayerContainer;
