import { Box, Button, Container, Divider, makeStyles, TextField, Typography } from '@material-ui/core';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { locationValidationSchema } from '../../validations/location-validation-schema.js';
import { mapRequests } from '../../services/map-requests.js';
import { utils } from '../../common/utils.js';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
  },
  alignCenter: {
    textAlign: 'center',
  },
  equalMargin: {
    margin: theme.spacing(1, 0, 2),
  },
}));

const TravelInfo = ({ routeInfo, setRouteInfo, locations, setLocations, locationsError, setLocationsError, setDisabledNext }) => {
  const schema = locationValidationSchema(yup);

  const formik = useFormik({
    initialValues: {
      origin: locations.origin,
      destination: locations.destination,
    },
    validationSchema: schema,
    onSubmit: mapRequests.getRoute(setRouteInfo, setLocationsError, setDisabledNext),
  });

  useEffect(() => {
    setLocations(formik.values);
  }, [formik.values]);

  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs" color="secondary">
      <form onSubmit={formik.handleSubmit} className={classes.form}>
        <TextField
          fullWidth
          required
          margin="normal"
          variant="outlined"
          id="origin"
          name="origin"
          label="Start"
          value={formik.values.origin}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.origin && Boolean(formik.errors.origin)}
          helperText={formik.touched.origin && formik.errors.origin}
        />
        <TextField
          fullWidth
          required
          margin="normal"
          variant="outlined"
          id="destination"
          name="destination"
          label="End"
          value={formik.values.destination}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.destination && Boolean(formik.errors.destination)}
          helperText={formik.touched.destination && formik.errors.destination}
        />
        <Button color="primary" variant="contained" fullWidth type="submit" className={classes.submit}>
          Get travel information
        </Button>
      </form>
      <Box>
        {routeInfo && (
          <div className={classes.alignCenter}>
            {/* <Typography display="inline">Start: </Typography> */}
            <Typography>
              {routeInfo.startLocation.formattedAddress} → {routeInfo.endLocation.formattedAddress} ({Math.round(routeInfo.travelDistance)}km)
            </Typography>
            <Typography variant="h4" className={classes.equalMargin}>
              <WatchLaterIcon /> {utils.formatTravelDuration(routeInfo.travelDuration)}
            </Typography>
            <Typography></Typography>
          </div>
        )}
        {locationsError && (
          <div>
            <Typography>{locationsError}</Typography>
          </div>
        )}
      </Box>
    </Container>
  );
};

export default TravelInfo;
