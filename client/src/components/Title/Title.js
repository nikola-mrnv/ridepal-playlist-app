import { Grid, Typography } from '@material-ui/core';
import React from 'react';

const Title = () => {
  return (
    <Grid style={{ padding: '4% 0' }}>
      <Typography component="h1" variant="h2" align="center"
        style={{ fontWeight: '600' }}
      >
        Build Custom Playlists
    </Typography >
      <Typography component="h1" variant="h2" align="center"
        style={{ color: '#DE541E', fontWeight: '600' }}
      >
        for Your Travels
    </Typography >
    </Grid>
  );
};

export default Title;
