import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import PlaylistSimple from '../PlaylistSimple/PlaylistSimple';
import { Grid, IconButton, Typography } from '@material-ui/core';
import Loading from '../Loading/Loading';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import AuthContext from '../../providers/authContext';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const PlaylistList = ({ user, playlists, setPlaylists, loading, withImage, fromProfile }) => {
  const history = useHistory();
  const auth = useContext(AuthContext);

  return (
    <>
      {loading ? (
        <Grid item xs={12}>
          <Loading />
        </Grid>
      ) : playlists.length === 0 ? (
        <Grid item xs={12}>
          <Typography variant="h5" gutterBottom align="center">
            No playlists to show...
          </Typography>
          <Typography variant="body2" align="center">
            {auth.user
              ? 'Create a new playlist'
              : 'Log in to create one'
            }
            <IconButton
              onClick={auth.user
                ? () => history.push('/new-playlist')
                : () => history.push('/login')
              }>
              {auth.user
                ? <AddCircleIcon fontSize="large" color="secondary" />
                : <ExitToAppIcon fontSize="large" color="secondary" />
              }
            </IconButton>
          </Typography>
        </Grid>
      ) : (
        <>
          {playlists.map((playlist) => (
            <Grid item xs={12} key={playlist.playlist_id}>
              <PlaylistSimple
                user={user}
                withImage={withImage}
                playlist={playlist}
                playlists={playlists}
                setPlaylists={setPlaylists}
                fromProfile={fromProfile}
              />
            </Grid>
          ))}
        </>
      )}
    </>
  );
};

export default PlaylistList;
