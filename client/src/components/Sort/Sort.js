import React, { useEffect, useState } from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Box } from '@material-ui/core';

const Sort = ({ sortPlaylists }) => {

  const [value, setValue] = useState('');

  useEffect(() => {
    sortPlaylists(value);
  }, [value]);

  return (
    <Box>
      <FormControl component="fieldset">
        <FormLabel component="legend">Sort by</FormLabel>
        <RadioGroup style={{ flexDirection: 'row' }} label="sort" name="sort" value={value} onChange={e => setValue(e.target.value)}>
          <FormControlLabel value="nameAsc" control={<Radio size='small' />} label="Name (A-Z)" />
          <FormControlLabel value="nameDesc" control={<Radio size='small' />} label="Name (Z-A)" />
          <FormControlLabel value="durationUp" control={<Radio size='small' />} label="Duration (↑)" />
          <FormControlLabel value="durationDown" control={<Radio size='small' />} label="Duration (↓)" />
          <FormControlLabel value="rankUp" control={<Radio size='small' />} label="Rank (↑)" />
          <FormControlLabel value="rankDown" control={<Radio size='small' />} label="Rank (↓)" />
        </RadioGroup>
      </FormControl>
    </Box>
  );

};

export default Sort;
