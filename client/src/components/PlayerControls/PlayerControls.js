import React, { useEffect, useState } from 'react';
import { Grid, IconButton, makeStyles, Typography } from '@material-ui/core';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import PauseIcon from '@material-ui/icons/Pause';

const useStyles = makeStyles((theme) => ({
  player: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  controls: {
    display: 'flex',
    justifyContent: 'space-evenly',
    width: '100%',
    alignItems: 'center',
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

const PlayerControls = ({ currentTrack, togglePlay, tracks, playing, handleSkip }) => {

  const classes = useStyles();

  const nextTrack = tracks.find(track => track.index === currentTrack.index + 1);
  const previousTrack = tracks.find(track => track.index === currentTrack.index - 1);

  return (
    <div className={classes.player}>
      <Grid >
        <Typography
          variant="body1"
          style={{ paddingTop: '16px' }}
        >
          Track {currentTrack.index + 1} - {currentTrack.title} - {currentTrack.artist_name}
        </Typography>
      </Grid>
      <Grid className={classes.controls}>
        <IconButton
          disabled={tracks[0].dz_track_id === currentTrack.dz_track_id}
          aria-label="previous"
          onClick={() => handleSkip(previousTrack)}
        >
          <SkipPreviousIcon />
        </IconButton>
        <IconButton onClick={togglePlay} aria-label="play/pause">
          {playing
            ? <PauseIcon className={classes.playIcon} />
            : <PlayArrowIcon className={classes.playIcon} />
          }
        </IconButton>
        <IconButton
          disabled={tracks[tracks.length - 1].dz_track_id === currentTrack.dz_track_id}
          aria-label="next"
          onClick={() => handleSkip(nextTrack)}
        >
          <SkipNextIcon />
        </IconButton>
      </Grid>

    </div >
  );
};

export default PlayerControls;
