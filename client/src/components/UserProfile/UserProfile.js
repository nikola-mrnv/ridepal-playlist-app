import { Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { userRequests } from '../../services/user-requests';
import Loading from '../Loading/Loading';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { updateProfileValidationSchema } from '../../validations/update-profile-validation-schema';
import UserAvatar from '../UserAvatar/UserAvatar';
import UserDetails from '../UserDetails/UserDetails';
import UserMessage from '../UserMessage/UserMessage';

const UserProfile = ({ user }) => {

  const [userData, setUserData] = useState({});
  const [loading, setLoading] = useState(true);
  const schema = updateProfileValidationSchema(yup);
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const formik = useFormik({
    initialValues: {
      display_name: '',
      email: '',
    },
    validationSchema: schema,
    onSubmit: userRequests.updateProfile(user, setLoading, setUserData, setUserMessage),
  });

  useEffect(() => {
    userRequests.getUserData(user, setLoading, setUserData, formik.values);
  }, []);

  const updateAvatar = avatar => {
    userRequests.updateAvatarImage(user, avatar, setLoading, userData, setUserData, setUserMessage);
  };

  return (
    <>
      <Typography
        component="h2"
        variant="h5"
        gutterBottom >
        My Profile
          </Typography>
      <Grid container spacing={3} style={{ padding: '12px' }}>
        <Grid item xs={5} sm={4}>
          {loading
            ? <Loading />
            : <Grid style={{ display: 'flex', flexDirection: 'column' }}>
              <UserAvatar userData={userData} updateAvatar={updateAvatar} />
            </Grid>}
        </Grid>
        <Grid item xs={7} sm={8}>
          {loading
            ? <Loading />
            : <UserDetails userData={userData} formik={formik} />
          }
        </Grid>
      </Grid>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </>
  );
};

export default UserProfile;
