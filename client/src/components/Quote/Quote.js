import { Avatar, Grid, makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import { grey } from '@material-ui/core/colors';
import { CONSTANTS } from '../../common/constants';

const useStyles = makeStyles({
  text: {
    fontStyle: 'italic',
    color: grey[600],
    textAlign: 'right',
  },
  name: {
    margin: 'auto 0',
  },
  avatar: {
    width: '60px',
    height: '60px',
    margin: 'auto 0',
    padding: '10px 20px',
  },
  avatarContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  quoteContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'right',
    padding: '3% 12px',
  },
});

const Quote = () => {

  const classes = useStyles();

  return (
    <Grid className={classes.quoteContainer}>
      <Typography
        component="h3"
        variant="h5"
        className={classes.text}>
        <FormatQuoteIcon />
          PlayPal is always with me on the road!
        <FormatQuoteIcon />
        <Grid className={classes.avatarContainer}>
          <Avatar
            variant='rounded'
            src={`${CONSTANTS.API_URL}/uploads/driverAvatar.jpg`}
            className={classes.avatar}
          />
          <Typography className={classes.name}>
            - Lilly, Truck Driver
          </Typography>
        </Grid>
      </Typography>
    </Grid>
  );
};

export default Quote;