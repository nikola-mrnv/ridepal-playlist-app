import React from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

const UserMessage = ({ open, handleClose, severity, message }) => {


  return (
    <Snackbar
      autoHideDuration={3000}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
      open={open}
      onClose={handleClose}>
      <Alert
        variant="filled"
        onClose={handleClose}
        severity={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default UserMessage;
