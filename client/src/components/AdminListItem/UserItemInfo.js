import { Grid, Typography } from '@material-ui/core';
import React from 'react';

const UserItemInfo = ({ user_id, username, display_name, role_id, is_deleted }) => {
  return (
    <Grid>
      <Typography>{`ID: ${user_id} / USERNAME: ${username} / DISPLAY NAME: ${display_name} / ROLE: ${role_id} DELETED: ${
        is_deleted ? 'YES' : 'NO'
      }`}</Typography>
    </Grid>
  );
};

export default UserItemInfo;
