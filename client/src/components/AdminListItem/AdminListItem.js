import React, { useState } from 'react';
import { Paper, IconButton, makeStyles, Grid, Collapse } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import RestoreFromTrashIcon from '@material-ui/icons/RestoreFromTrash';
import PlaylistItemInfo from './PlaylistItemInfo.js';
import UserItemInfo from './UserItemInfo.js';
import PlaylistItemEdit from './PlaylistItemEdit.js';
import UserItemEdit from './UserItemEdit.js';
import DialogDeleteItem from '../DialogDeleteItem/DialogDeleteItem.js';
import UserMessage from '../UserMessage/UserMessage.js';
import { adminRequests } from '../../services/admin-requests';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    color: theme.palette.text.secondary,
    backgroundColor: '#eee',
  },
  paperDeleted: {
    padding: theme.spacing(1),
    color: theme.palette.text.secondary,
    backgroundColor: '#e89595',
  },
}));

const AdminListItem = (props) => {
  const [alertDialog, setAlertDialog] = useState(false);
  const [expanded, setExpanded] = useState(false);

  const { playlists, setPlaylists, users, setUsers } = props;
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const handleClickOpen = () => {
    setAlertDialog(true);
  };

  const handleClickClose = () => {
    setAlertDialog(false);
  };

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleDeletePlaylist = (playlistId) => {
    setAlertDialog(false);
    adminRequests.deletePlaylistRequest(playlistId, playlists, setPlaylists, setUserMessage);
  };

  const handleDeleteUser = (userId) => {
    setAlertDialog(false);
    adminRequests.deleteUserRequest(userId, users, setUsers, setUserMessage);
  };

  const classes = useStyles();
  return (
    <div>
      <Paper className={props.is_deleted ? classes.paperDeleted : classes.paper}>
        <Grid container justify="space-between" alignItems="center">
          {props.itemInfo === 'playlist' && <PlaylistItemInfo {...props} />}
          {props.itemInfo === 'user' && <UserItemInfo {...props} />}
          <Grid>
            <IconButton onClick={handleExpandClick} aria-expanded={expanded} aria-label="edit">
              <EditIcon />
            </IconButton>
            {props.is_deleted ? (
              <IconButton aria-label="restore">
                <RestoreFromTrashIcon />
              </IconButton>
            ) : (
              <>
                <IconButton aria-label="delete" onClick={handleClickOpen}>
                  <DeleteIcon />
                </IconButton>
                <DialogDeleteItem
                  {...props}
                  alertDialog={alertDialog}
                  handleClickClose={handleClickClose}
                  handleDeletePlaylist={handleDeletePlaylist}
                  handleDeleteUser={handleDeleteUser}
                />
              </>
            )}
          </Grid>
        </Grid>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          {props.itemInfo === 'playlist' && <PlaylistItemEdit {...props} setExpanded={setExpanded} />}
          {props.itemInfo === 'user' && <UserItemEdit {...props} setExpanded={setExpanded} />}
        </Collapse>
      </Paper>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </div>
  );
};

export default AdminListItem;
