import { Button, Grid, TextField } from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { updateProfileValidationSchema } from '../../validations/update-profile-validation-schema.js';
import React, { useState } from 'react';
import { userRequests } from '../../services/user-requests.js';
import UserMessage from '../UserMessage/UserMessage.js';

const UserItemEdit = (props) => {
  const { user_id, display_name, email, users, setUsers, setExpanded } = props;
  const schema = updateProfileValidationSchema(yup);
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const formik = useFormik({
    initialValues: {
      display_name: display_name,
      email: email,
    },
    validationSchema: schema,
    onSubmit: userRequests.editUser(user_id, users, setUsers, setExpanded, setUserMessage),
  });
  return (
    <Grid container spacing={3} alignItems="center">
      <Grid item xs={5}>
        <TextField
          fullWidth
          margin="normal"
          id="display_name"
          name="display_name"
          label="Display name"
          value={formik.values.display_name}
          onChange={formik.handleChange}
          error={formik.touched.display_name && Boolean(formik.errors.display_name)}
          helperText={formik.touched.display_name && formik.errors.display_name}
          disabled={props.is_deleted === 1 ? true : false}
        />
      </Grid>
      <Grid item xs={5}>
        <TextField
          fullWidth
          margin="normal"
          id="email"
          name="email"
          label="Email"
          value={formik.values.email}
          onChange={formik.handleChange}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
          disabled={props.is_deleted === 1 ? true : false}
        />
      </Grid>
      <Grid item xs={2} align="center">
        <Button color="primary" variant="contained" disabled={props.is_deleted === 1} onClick={formik.handleSubmit}>
          Save and Close
        </Button>
      </Grid>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </Grid>
  );
};

export default UserItemEdit;
