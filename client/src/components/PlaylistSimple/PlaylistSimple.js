import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Avatar, Button, Checkbox, Chip, Collapse, FormControlLabel, Grid, Icon, IconButton, TextField } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { utils } from '../../common/utils';
import PlayerContainer from '../PlayerContainer/PlayerContainer';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { playlistRequests } from '../../services/playlist-requests.js';
import { editPlaylistValidationSchema } from '../../validations/edit-playlist-validation-schema.js';
import DialogDeleteItem from '../DialogDeleteItem/DialogDeleteItem.js';
import { adminRequests } from '../../services/admin-requests.js';
import UserMessage from '../UserMessage/UserMessage';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: '30%',
  },
  spaceBetween: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  expand: {
    width: '100%',
    backgroundColor: '#c3c3bc',
    transform: 'rotate(0deg)',
    hover: {
      backgroundColor: '#c3c3bc',
    },
  },
  expandOpen: {
    width: '100%',
    backgroundColor: '#c3c3bc',
    transform: 'rotate(180deg)',
  },
}));

const PlaylistSimple = ({ user, playlist, playlists, setPlaylists, withImage, fromProfile }) => {
  const classes = useStyles();
  const formattedDuration = utils.formatDuration(playlist.duration);
  const [expanded, setExpanded] = useState(false);
  const [editPlaylist, setEditPlaylist] = useState(false);
  const [hidden, setHidden] = useState(playlist.is_hidden);
  const [alertDialog, setAlertDialog] = useState(false);
  const schema = editPlaylistValidationSchema(yup);
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: '',
  });

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const formik = useFormik({
    initialValues: {
      name: playlist.name,
    },
    validationSchema: schema,
    onSubmit: playlistRequests.editPlaylist(playlist.playlist_id, hidden, playlists, setPlaylists, setExpanded, setUserMessage),
  });

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleChangeHidden = (e) => setHidden(Number(e.target.checked));

  const handleSave = (e) => {
    setEditPlaylist(false);
    formik.handleSubmit(e);
  };

  const handleClickOpen = () => {
    setAlertDialog(true);
  };

  const handleClickClose = () => {
    setAlertDialog(false);
  };

  const handleDeletePlaylist = (playlistId = playlist.playlist_id) => {
    setAlertDialog(false);
    adminRequests.deletePlaylistRequest(playlistId, playlists, setPlaylists, setUserMessage);
  };

  return (
    <>
      <Card className={classes.root}>
        {withImage && <CardMedia className={classes.cover} image={playlist.picture_url} title={playlist.name} />}
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Grid container justify="space-between">
              {editPlaylist ? (
                <Grid item xs={8}>
                  <TextField
                    fullWidth
                    id="name"
                    name="name"
                    label="Playlist name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                  />
                  <FormControlLabel
                    control={<Checkbox checked={Boolean(hidden)} onChange={handleChangeHidden} name="hidden" disabled={playlist.is_deleted === 1} />}
                    label="Hidden"
                  />
                </Grid>
              ) : (
                <Grid item xs={8}>
                  <Typography component="h6" variant="h6" display="inline" alignContent="center">
                    {playlist.is_hidden === 1 && <VisibilityOffIcon fontSize="large" color="disabled" />}
                    {`${playlist.name}`}
                  </Typography>
                </Grid>
              )}
              <Grid item xs={2}>
                {user && user.user_id === playlist.user_id && fromProfile && (
                  <div style={{ justifyContent: 'space-between' }}>
                    {editPlaylist ? (
                      <IconButton onClick={handleSave}>
                        <SaveIcon />
                      </IconButton>
                    ) : (
                      <IconButton onClick={() => setEditPlaylist(true)}>
                        <EditIcon />
                      </IconButton>
                    )}
                    <IconButton onClick={handleClickOpen}>
                      <DeleteIcon />
                    </IconButton>
                    <DialogDeleteItem
                      itemInfo="playlist"
                      playlist_id={playlist.playlist_id}
                      alertDialog={alertDialog}
                      handleClickClose={handleClickClose}
                      handleDeletePlaylist={handleDeletePlaylist}
                    />
                  </div>
                )}
              </Grid>
              <Typography variant="subtitle1" color="textSecondary">
                {formattedDuration}
              </Typography>
            </Grid>
            <Typography variant="subtitle1" color="textSecondary">
              Rank: {playlist.playlist_rank.toLocaleString()}
            </Typography>
            {playlist.genres.map((genre, index) => (
              <Chip key={index} label={`${genre.name} ${genre.amount}%`} avatar={<Avatar src={genre.picture_url} />} />
            ))}
          </CardContent>
          <div style={{ backgroundColor: '#c3c3bc' }}>
            <Button
              className={expanded ? classes.expandOpen : classes.expand}
              onClick={() => handleExpandClick(playlist.playlist_id)}
              aria-expanded={expanded}
              aria-label="show tracks"
            >
              <ExpandMoreIcon style={{ color: 'white' }} />
            </Button>
          </div>
        </div>
      </Card>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <PlayerContainer playlistId={playlist.playlist_id} />
      </Collapse>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </>
  );
};

export default PlaylistSimple;
