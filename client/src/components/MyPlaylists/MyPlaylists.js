import { Avatar, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import PlaylistList from '../PlaylistList/PlaylistList';
import { playlistRequests } from '../../services/playlist-requests';

const useStyles = makeStyles({
  paper: {
    margin: 0,
    alignContent: 'flex-start',
  },
});

const MyPlaylists = ({ user, withImage, fromProfile }) => {
  const classes = useStyles();
  const [playlists, setPlaylists] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    playlistRequests.getUserPlaylists(user, setPlaylists, setLoading);
  }, []);

  return (
    <>
      <Grid item container xs={12} spacing={3} className={classes.paper}>
        <Typography component="h2" variant="h5" gutterBottom>
          My Playlists
        </Typography>
        <PlaylistList
          user={user}
          loading={loading}
          playlists={playlists.filter((playlist) => playlist.is_deleted !== 1)}
          setPlaylists={setPlaylists}
          withImage={withImage}
          fromProfile={fromProfile}
        />
      </Grid>
    </>
  );
};

export default MyPlaylists;
