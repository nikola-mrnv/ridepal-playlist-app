import jwtDecode from 'jwt-decode';
import { CONSTANTS } from '../common/constants';
import { getToken } from '../providers/authContext.js';

export const handleLogout = (auth) => {
  fetch(`${CONSTANTS.API_URL}/users/logout`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      localStorage.removeItem('token');
      auth.setAuthState({
        isLoggedIn: false,
        user: null,
      });
    });
};

export const onSubmitRegister = (history, setUserMessage) => (values) => {
  fetch(`${CONSTANTS.API_URL}/users/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  })
    .then((response) => response.json())
    .then((result) => {
      if (!result.error) {

        setUserMessage({
          open: true,
          severity: 'success',
          message: 'Thank you for registering! You can now log in.',
        });
        setTimeout(() => history.push('/login'), 2000);
      } else {
        throw new Error(result.error);
      }
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }));
};

export const onSubmitLogin = (auth, history, setUserMessage) => (values) => {
  fetch(`${CONSTANTS.API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  })
    .then((response) => response.json())
    .then(({ token }) => {
      try {
        localStorage.setItem('token', token);
        const user = jwtDecode(token);
        auth.setAuthState({ user, isLoggedIn: true });
        history.push('/');
      } catch (error) {
        throw new Error('Invalid login credentials!');
      }
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }));
};

const getUserData = (user, setLoading, setUserData, formValues) => {
  fetch(`${CONSTANTS.API_URL}/users/${user.user_id}`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  })
    .then((res) => res.json())
    .then((data) => {
      if (!data.error) {
        setUserData(data);
        formValues.display_name = data.display_name;
        formValues.email = data.email;
      } else {
        throw new Error(data.error);
      }
    })
    .catch((error) => console.log(error))
    .finally(() => setLoading(false));
};

const updateProfile = (user, setLoading, setUserData, setUserMessage) => (values) => {
  fetch(`${CONSTANTS.API_URL}/users/${user.user_id}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  })
    .then((res) => res.json())
    .then((data) => {
      if (!data.error) {
        setUserData(data);
        setUserMessage({
          open: true,
          severity: 'success',
          message: 'Profile updated!',
        });
      } else {
        throw new Error(data.error);
      }
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }))
    .finally(() => setLoading(false));
};

const updateAvatarImage = (user, avatar, setLoading, userData, setUserData, setUserMessage) => {
  const formData = new FormData();
  formData.append('avatar', avatar, avatar.name);
  fetch(`${CONSTANTS.API_URL}/users/${user.user_id}/avatar`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
    body: formData,
  })
    .then((res) => res.json())
    .then((data) => {
      if (!data.error) {
        setUserData({ ...userData, avatar_url: data.updatedAvatar });
        setUserMessage({
          open: true,
          severity: 'success',
          message: 'Avatar uploaded',
        });
      } else {
        throw new Error(data.error);
      }
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }))
    .finally(() => setLoading(false));
};

const editUser = (user_id, users, setUsers, setExpanded, setUserMessage) => (values) => {
  fetch(`${CONSTANTS.API_URL}/users/${user_id}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  })
    .then((response) => response.json())
    .then((data) => {
      const updatedUser = data;
      const updatedUsers = users.map((user) => (user.user_id === data.user_id ? updatedUser : user));
      setUsers(updatedUsers);
      setUserMessage({
        open: true,
        severity: 'success',
        message: 'User updated!',
      });
      setTimeout(() => setExpanded(false), 1500);
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }));
};

export const userRequests = {
  getUserData,
  updateProfile,
  updateAvatarImage,
  editUser,
};
