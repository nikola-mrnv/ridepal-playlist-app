import { getToken } from '../providers/authContext.js';
import { CONSTANTS } from '../common/constants.js';
const getRoute = (setRouteInfo, setLocationsError, setDisabledNext) => (values) => {
  fetch(`${CONSTANTS.API_URL}/maps/route`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(values),
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.message) {
        setLocationsError(data.message);
        setRouteInfo(null);
      } else {
        setRouteInfo(data);
        setLocationsError(null);
        setDisabledNext(false);
      }
    })
    .catch(console.warn);
};

export const mapRequests = {
  getRoute,
};
