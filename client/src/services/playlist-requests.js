import { CONSTANTS } from '../common/constants';
import { getToken } from '../providers/authContext';

const getAllPlaylists = (setPlaylists, setLoading, setInitialPlaylists) => {
  fetch(`${CONSTANTS.API_URL}/playlists`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  })
    .then((res) => res.json())
    .then((data) => {
      if (Array.isArray(data)) {
        setPlaylists(data);
        setInitialPlaylists(data);
      } else {
        throw new Error(data.error);
      }
    })
    .catch((error) => console.log(error))
    .finally(() => setLoading(false));
};

const getPlaylistById = (playlistId, setPlaylist, setLoading) => {
  fetch(`${CONSTANTS.API_URL}/playlists/${playlistId}`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  })
    .then((res) => res.json())
    .then((data) => {
      if (!data.error) {
        setPlaylist(data);
      } else {
        throw new Error(data.error);
      }
    })
    .catch((error) => console.log(error))
    .finally(() => setLoading(false));
};

const getUserPlaylists = (user, setPlaylists, setLoading) => {
  fetch(`${CONSTANTS.API_URL}/users/${user.user_id}/playlists`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  })
    .then((res) => res.json())
    .then((data) => {
      if (!data.error) {
        setPlaylists(data);
      } else {
        throw new Error(data.error);
      }
    })
    .catch((error) => console.warn(error))
    .finally(() => setLoading(false));
};

const getGenres = (setGenres, setLoading) => {
  fetch(`${CONSTANTS.API_URL}/playlists/genres`)
    .then((res) => res.json())
    .then((data) => {
      if (!data.message) {
        setGenres(data);
      } else {
        throw new Error(data.message);
      }
    })
    .catch((error) => console.log(error))
    .finally(() => setLoading(false));
};

const getGenreCheckboxes = (setCheckboxes, setDisabledNext) => {
  fetch(`${CONSTANTS.API_URL}/playlists/genres`)
    .then((response) => response.json())
    .then((data) => {
      const transformedGenres = data.map((genre) => ({
        dz_genre_id: genre.dz_genre_id,
        name: genre.name,
        average_duration: genre.average_duration,
        checked: false,
        disabled: false,
      }));
      setCheckboxes(transformedGenres);
      setDisabledNext(true);
    })
    .catch(console.warn);
};

const generateNewPlaylist = (payload, setNewPlaylist) => {
  fetch(`${CONSTANTS.API_URL}/playlists/generator`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  })
    .then((response) => response.json())
    .then((data) => {
      setNewPlaylist(data);
    })
    .catch(console.warn);
};

const saveNewPlaylist = (newPlaylist, hidden, picture_url, history, setUserMessage) => (values) => {

  fetch(`${CONSTANTS.API_URL}/playlists/new-playlist`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify({ ...values, ...newPlaylist, hidden: hidden, picture_url: picture_url }),
  })
    .then((response) => response.json())
    .then((data) => {
      setUserMessage({
        open: true,
        severity: 'success',
        message: `${data.message}`,
      });
      setTimeout(() => history.push('/'), 2000);
    })
    .catch(console.warn);
};

const setPlaylistPicture = (setImage) => {
  fetch(`${CONSTANTS.UNSPLASH_API_URL}/photos/random/?client_id=${CONSTANTS.UNSPLASH_API_KEY}&query=music`)
    .then((response) => response.json())
    .then((data) => setImage(data.urls.small))
    .catch(console.warn);
};

const editPlaylist = (playlist_id, hidden, playlists, setPlaylists, setExpanded, setUserMessage) => (values) => {
  fetch(`${CONSTANTS.API_URL}/playlists/${playlist_id}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ ...values, hidden: hidden }),
  })
    .then((response) => response.json())
    .then((data) => {
      const updatedPlaylist = data;
      const updatedPlaylists = playlists.map((playlist) => (playlist.playlist_id === data.playlist_id ? updatedPlaylist : playlist));
      setPlaylists(updatedPlaylists);
      setUserMessage({
        open: true,
        severity: 'success',
        message: 'Playlist updated!',
      });
      setTimeout(() => setExpanded(false), 1500);
    })
    .catch(error => setUserMessage({
      open: true,
      severity: 'error',
      message: `${error.message}`,
    }));
};

export const playlistRequests = {
  getAllPlaylists,
  getPlaylistById,
  getUserPlaylists,
  getGenres,
  getGenreCheckboxes,
  generateNewPlaylist,
  saveNewPlaylist,
  setPlaylistPicture,
  editPlaylist,
};
