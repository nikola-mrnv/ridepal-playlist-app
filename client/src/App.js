import './App.css';
import AuthContext, { extractUser, getToken } from './providers/authContext';
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './pages/Login/Login.js';
import Register from './pages/Register/Register.js';
import GuardedRoute from './components/hof/GuardedRoute';
import Home from './pages/Home/Home';
import Admin from './pages/Admin/Admin.js';
import NewPlaylist from './pages/NewPlaylist/NewPlaylist.js';
import MyProfile from './pages/MyProfile/MyProfile';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#3F3F37',
    },
    secondary: {
      main: '#DE541E',
    },
  },
  typography: {
    fontFamily: 'Lato',
  },
});

function App() {
  const token = getToken();
  const [authValue, setAuthState] = useState({
    isLoggedIn: Boolean(extractUser(token)),
    user: extractUser(token),
  });

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <AuthContext.Provider value={{ ...authValue, setAuthState }}>
          <div className="App">
            <Switch>
              <Route path="/" exact component={Home} />
              <GuardedRoute path="/login" component={Login} isLoggedIn={!authValue.isLoggedIn} />
              <GuardedRoute path="/register" component={Register} isLoggedIn={!authValue.isLoggedIn} />
              <GuardedRoute path="/myprofile" component={MyProfile} isLoggedIn={authValue.isLoggedIn} />
              <GuardedRoute path="/admin" component={Admin} isLoggedIn={authValue.user && authValue.user.role_id === 1} />
              <GuardedRoute path="/new-playlist" component={NewPlaylist} isLoggedIn={authValue.isLoggedIn} />
            </Switch>
          </div>
        </AuthContext.Provider>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
